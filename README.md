# README #

This is a Social Network Application on Facebook. 
This app features the non-trivial use of data via the GAE datastore. 
For this a Twitter application is implemented. 
You can collect data from the user and store all TweetFriends in the GAE datastore. 
You should be using data from the datastore as well as collecting new data from the user. 
Along with tweets the user is presented with their name, profile image all from Facebook api call. 
Also, these features are implemented:
	1) post the tweet on their facebook page/timeline
	2) send a direct message to friends with the tweet they just made as a content.